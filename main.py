from datetime import datetime


class Warehouse():
    def __init__(self, capacity):
        self.capacity = capacity
        self.items = list()
        self.status = "Available"

    def add_product(self, *product):
        if self.status != "Available":
            return "Warehouse Full or Closed"
        if len(product) == 1:
            self.capacity -= product.amount
            if self.capacity > 0:
                self.items.append(product)
            else:
                self.status = "Not Available"
        else:
            for i in product:
                self.capacity -= product.amount
                if self.capacity > 0:
                    self.items.append(i)
                else:
                    self.status = "Not Available"
                    break

    def buy_product(self, to_buy, how_wuch):
        if to_buy in self.items:
            to_buy.amount -= how_wuch
            price = to_buy.price * how_wuch
            return price
        else:
            return False


    @property
    def show_all(self):
        for i in self.items:
            print(i.name, i.amount, i.price)

    def close_warehows(self):
        for product in self.items:
            product.shelf_life -= 1
        self.status = "Not available"

    def open_warehows(self):
        for product in self.items:
            if product.shelf_life == 0:
                del product
        if self.capacity > 0:
            self.status = "Available"


class Product:
    def __init__(self, name, amount, price, ability, shelf_life):
        self.name = name
        self.amount = amount
        self.price = price
        self.ability = ability
        self.shelf_life = shelf_life

    @property
    def display_count(self):
        print(f'Осталось в наличии {self.name}: {self.amount} шт.')

    @property
    def display_price(self):
        print(f'Цена {self.name}: {self.price}')

    @property
    def check_storage_life(self):
        if self.shelf_life > 0:
            print(f"Срок годности {self.name} впорядке")


class OlderThan18(Product):
    def check_of_access(self, bith_date_of_custumer):
        today = datetime.datetime.now()
        delta = today - bith_date_of_custumer
        if round(delta.days / 365) >= 18:
            return True


class Basket():
    def __init__(self):
        self.basket = []
        self.final_price = 0
        self.quantity_of_goods = 0

    def Adding(self, product):
        self.basket.append(product)
        self.final_price += product.price

    def removal_of_goods(self, product):
        self.basket.remove(product)

    @property
    def get_price(self):
        return self.final_price


class Custumer():

    def __init__(self, name, date_of_birth):
        self.__name = name
        self.date_of_birth = datetime.datetime(date_of_birth)
        self.bonuses = 0
        self.preferences = {}

    def accrual_of_bonuses(self, purchase_amount):
        if purchase_amount <= 1000:
            self.bonuses += purchase_amount * 0.01
        elif 1000 < purchase_amount <= 5000:
            self.bonuses += purchase_amount * 0.03
        else:
            self.bonuses += purchase_amount * 0.05

    def creating_a_preference_base(self, product_manufacturer_names):
        for i in product_manufacturer_names:
            if i in self.preferences:
                self.preferences[i] += 1
            else:
                self.preferences[i] = 1

    def waste_of_bonuses(self, purchase_amount):
        if purchase_amount > self.bonuses:
            purchase_amount -= self.bonuses
            self.bonuses = 0
            return purchase_amount
        else:
            self.bonuses -= purchase_amount
            return 0

    def init_bucket(self):
        self.shopcurt = Basket()
